# nd6  - the world's best dice-roller

## Commands

Any dice roll of the form `3d6-1` will do fine.
You can also miss out parts of a roll, and all values will take the previous values as a default.

Just type `d10`, and if the previous roll was `2d6`, the new one will be `2d10`.
Then you can type `3`, and the roll will be `3d10`.
Then use `+1`, and the roll will be `3d10+1`.

You can input a target number (TN), and you will see if you got above or below the TN.
To remove the TN, just set it to 0.

You can do the same with difficulty, letting you use dicepools for White Wolf games.

## Store Commands

Store a commands by setting them with:

> attack is 1d20+4

Thereafter you can say 'attack', and the program runs 1d20+4.

Use `store` to see your store.

## Setting Commands

All sensible syntax will work.

- tn 17
- diff is 8
- difficulty = 5
- TN=5

## Making BIND characters

Type in `character`, and you'll get a [BIND](https://gitlab.com/bindrpg/core) character.

## Setup


```bash
make
```
