#!/usr/bin/python3

import re
import yaml
from termcolor import colored
from random import randint
from diceHelp import *
from chargen import diceRoll, chargen
from test import *
from store import *
from average import *
from area import *

title = '''

           _  __   
 _ __   __| |/ /_  
| '_ \\ / _` | '_ \\ 
| | | | (_| | (_) |
|_| |_|\\__,_|\\___/ 
                   
'''

title = colored(title, "red")

print(title)

prompt = colored("\tRoll the dice\n", "blue", attrs=["underline"])

nod = 2  # number of dice
tod = 6  # type of die, e.g. '8' means 'D8'
mod = 0  # modifier, e.g. '+3' or '-1'
diff = 0  # difficulty rating, for WW-like games
tn = 0  # target number, e.g. the number or successes required

# this one detects rolls in anything like '2d6', or just '2' or ''.
# we use it to get the roll out of a string when some parts are empty
rollRegex = re.compile(r"(\d*)d?(\d*)\s*((\-|\+)\s*\d*)*")

# this is the same, except it requires a 'd'
detectRollRegex = re.compile(r"(\d*)[d|\+|\-](\d*)\s*((\-|\+)\s*\d*)*")

# this one just checks difficulty numbers for WW games, e.g. 'diff 8' or 'difficulty = 4'
designatorRegex = re.compile(r"(dif|tn){1}\D*(\d+)")

storeRegex = re.compile(r"(\w+)\s*(is|=)\s*(\d+d\d+\s*[\-|\+\s*\d]*)")


def process(diceRolls):
    if int(diff) < 2:
        return sum(diceRolls)
    else:
        total = 0
        for number in diceRolls:
            if int(number) >= int(diff):
                total += 1
        if int(number) == 1 and int(total) == 0:
            total -= 1
        return total

def declare(total):
    result = "Result: " + colored(str(total), "white", attrs=["bold"])
    if int(diff) > 2:
        if total < 0:
            result += " " + colored("(Botch)", "red", attrs=["underline"])
        elif total == 0:
            result += " " + colored("successes", "magenta")
        elif abs(total) == 1:
            result += " " + colored("success", "yellow")
        elif abs(total) > 3:
            result += " " + colored("successes", "green", attrs=["underline"])
        else:
            result += " " + colored("successes", "green")
    if int(tn) > 0:
        if int(tn) <= int(total):
            result += " " + colored("(Success)", "green", attrs=[])
        else:
            result += " " + colored("(Failure)", "red")
    print(result)


def roll(nod, tod, mod, command):
    global avList
    structuredRoll = rollRegex.search(command)
    diceRolls = []

    if structuredRoll.group(1) != "":
        nod = structuredRoll.group(1)
        # if the number of dice changes, the modifier probably no longer applies
        mod = 0
    if structuredRoll.group(2) != "":
        tod = structuredRoll.group(2)
        if int(tod) < 1:
            print("Nothing ain't random")
            tod = 6
        # if the type of dice changes, the modifier probably no longer applies
        mod = 0
    rollSummary = "Roll: " + str(nod) + "D" + str(tod)
    for die in range(0, int(nod)):
        diceRolls.append(randint(1, int(tod)))
    total = process(diceRolls)
    if str(structuredRoll.group(3)) != "None":
        mod = structuredRoll.group(3)
        mod = mod.replace(
            " ", ""
        )  # if the user typed in ' + 3 ' we need to remove that space
    if int(mod) != 0:
        rollSummary += str(mod)
    total += int(mod)  # with all the spaces gone, we can treat this as a number
    rollSummary = colored(rollSummary, "cyan")
    avList.append(total)
    print(rollSummary)
    print("Dice: " + str(diceRolls))
    declare(total)

    return nod, tod, mod


print("Type 'help' for help")


def testall():
    for x in range(0, len(testRun)):
        print(testRun[x])
        action(testRun[x])


def action(command):
    global diff, tn, nod, tod, mod, currentHelpItem
    command = command.lower()
    if randint(1, 500) == 1:
        print("You are likely to be eaten by a grue")
    if command == "" or command[0].isdigit():
        nod, tod, mod = roll(nod, tod, mod, command)
    elif command.startswith("h"):
        showHelp()
    elif command == ("test"):
        testall()
    elif command == ("av"):
        showAv()
    elif command.startswith("cha"):
        chargen()
    elif command.startswith("area"):
        makeLand()
    elif designatorRegex.search(command):
        diffStructured = designatorRegex.search(command)
        if diffStructured.group(1).startswith("dif"):
            diff = diffStructured.group(2)
            print("Difficulty set to " + diff)
        else:
            tn = diffStructured.group(2)
            print("TN set to " + str(tn))
    elif storeRegex.search(command):
        storeStructured = storeRegex.search(command)
        store.update({storeStructured.group(1): storeStructured.group(3)})
        print(
            'Stored "' + storeStructured.group(1) + '" as ' + storeStructured.group(3)
        )
    elif command in store:
        nod, tod, mod = roll(nod, tod, mod, store[command])
    elif command == "store":
        print(colored("Stored Values:", "green", attrs=["bold"]))
        print(yaml.dump(store, sort_keys=False, default_flow_style=False))
    elif detectRollRegex.search(command):
        nod, tod, mod = roll(nod, tod, mod, command)
    else:
        currentHelpItem = confused(currentHelpItem)


while True:
    action(input(prompt))

quit()
