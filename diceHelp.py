#!/usr/bin/python3
from termcolor import colored


def showHelp():
    print(
        colored(
            """

 _   _ _____ _     ____  
| | | | ____| |   |  _ \ 
| |_| |  _| | |   | |_) |
|  _  | |___| |___|  __/ 
|_| |_|_____|_____|_|    
    """,
            "blue",
        )
    )
    print(
        """
Roll dice like this:
    > 2d6
    > 3d4 - 1
    > d10+3
    > 4
    > +3

The dice roller will remember which type of dice you use, so you don't have to specify each time.  If you type '3d10', this rolls 3 10-sided dice. After that, you can just type '8', and it will roll 8 10-sided dice.

Set a target number like this:

    > tn 10

For White Wolf games, set a difficulty for the roll like this:

    > diff 8

Reset those number by making them '0', like this:

    > diff 0

Store commands, like an initiative roll of '1D20+4' like this:

    > init is 1d20+4

Then roll initiative like this:

    > init
    """
    )


currentHelpItem = 0
helpList = [
    "Confused",
    "Unclear",
    "Read the help page",
    "Here",
    "Leave the poor computer alone, and go touch grass",
]


def confused(currentHelpItem):
    print(helpList[currentHelpItem])
    if currentHelpItem == 3:
        showHelp()
    if currentHelpItem < len(helpList) - 1:
        currentHelpItem += 1
    return currentHelpItem
