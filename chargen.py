#!/usr/bin/python3

from random import randint
from termcolor import colored
from pointBuy import *


def d6():
    return randint(1, 6)


def diceRoll():
    return randint(1, 6) + randint(1, 6)


def attrRoll():
    roll = randint(1, 6) + randint(1, 6)
    if roll < 3:
        result = -3
    elif roll < 4:
        result = -2
    elif roll < 6:
        result = -1
    elif roll < 9:
        result = 0
    elif roll < 11:
        result = 1
    elif roll < 12:
        result = 2
    else:
        result = 3
    return result


def chargen():
    STR = attrRoll()
    DEX = attrRoll()
    SPD = attrRoll()
    INT = attrRoll()
    WTS = attrRoll()
    CHA = attrRoll()

    raceRoll = d6() + d6()
    race = "dwarf"
    if raceRoll < 4:
        race = "Gnoll"
        STR += 1
        SPD += 1
        INT -= 1
        CHA -= 2
    elif raceRoll < 6:
        race = "Dwarf"
        DEX += 1
        SPD -= 1
    elif raceRoll < 9:
        race = "Human"
        STR += 1
        WTS -= 1
    elif raceRoll < 11:
        race = "Elf"
        STR -= 1
        WTS += 1
    else:
        race = "Gnome"
        STR -= 2
        SPD -= 1
        DEX += 1
        INT += 1

    roles = {
        "fighter": STR + DEX + SPD,
        "mage": INT + WTS,
        "rogue": DEX + SPD + abs(CHA),
    }
    job = max(roles, key=roles.get)

    race = colored(race, "red")
    job = colored(job, "green")
    STR = colored(STR, "blue")
    DEX = colored(DEX, "blue")
    SPD = colored(SPD, "blue")
    INT = colored(INT, "blue")
    WTS = colored(WTS, "blue")
    CHA = colored(CHA, "blue")

    print("Race: " + race)
    print("Class: " + job)
    print("Strength: " + str(STR))
    print("Dexterity: " + str(DEX))
    print("Speed: " + str(SPD))
    print("Intelligence: " + str(INT))
    print("Wits: " + str(WTS))
    print("Charisma: " + str(CHA))
