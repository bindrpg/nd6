XP = 50

skills = {
    "Academics": 0,
    "Athletics": 0,
    "Performance": 0,
    "Tactics": 0,
}

attCost = {
    -5: 5,
    -4: 5,
    -3: 5,
    -2: 5,
    -1: 5,
    1: 10,
    2: 20,
    3: 40,
    4: 80,
}

skillCost = {
    1: 5,
    2: 10,
    3: 15,
}


def buyAttribute(ATT, XP=XP):
    if XP >= attCost[ATT]:
        XP -= attCost[ATT]
        ATT += 1
        return ATT, XP


def buySkill(SKILL, XP=XP):
    if XP >= skillCost[SKILL]:
        XP -= skillCost[SKILL]
        SKILL += 1
        return SKILL, XP
