from random import randint

lands = {
    1: 'mountain surrounded by rolling hills',
    2: 'impenetrable forest',
    3: 'coastal beach',
    4: 'open plane',
    5: 'swampland',
    6: 'archipelago',
}

rulers = {
    1: '1D3 + 2 factions, each ready to kill the others (re-roll on this table with +1)',
    2: 'a warlord, amassing an army to expand their power',
    3: 'a sorcerer king with no experience or business ruling',
    4: 'an opulent noble who demands half the wealth of all who enter',
    5: 'a lich who demands all the dead in the area',
    6: 'a spoilt noble who has never seen a peasant',
    7: 'a thieves guild with members throughout the local guilds',
}

race = {
    2: 'Gnomes',
    3: 'Gnolls',
    4: 'Dwarves',
    5: 'Elves',
    6: 'Humans',
}

def makeLand():
    x = randint(1,6)
    y = randint(1,6)
    chosenLand = lands[x]
    if y == 1:
        chosenRuler = 'quarallinc factions: '
        noFactions = randint(3,5)
        for faction in range(noFactions):
            chosenRuler += rulers[randint(2,7)] + ', '
    else:
        chosenRuler = rulers[y]
    z = x + y
    if z > 6:
        z = 6
    chosenRace = race[z]
    print('This ' + chosenLand + ' is populated by ' + chosenRace + ', and ruled by ' + chosenRuler)

